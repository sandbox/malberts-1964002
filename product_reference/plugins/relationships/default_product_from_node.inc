<?php

/**
 * @file
 * Extra plugin handlers for extra Commerce Product Reference.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Default Product from Content'),
  'keyword' => 'product',
  'description' => t('Adds the default Product from the Content context.'),
  'required context' => new ctools_context_required(t('Content'), 'entity:node'),
  'context' => 'commerce_product_reference_ctools_extra_default_product_from_node_context',
  'edit form' => 'commerce_product_reference_ctools_extra_default_product_from_node_settings_form',
  'defaults' => array('field' => FALSE),
);

/**
 * Return a new context based on an existing context.
 */
function commerce_product_reference_ctools_extra_default_product_from_node_context($context, $conf) {
  // If unset it wants a generic, unfilled context, which is just NULL.
  if (empty($context->data) || empty($context->data->{$conf['field']})) {
    return ctools_context_create_empty('entity:commerce_product', NULL);
  }

  $wrapper = entity_metadata_wrapper('node', $context->data);

  $field_name = $conf['field'];
  $field = field_info_field($conf['field']);

  if ($field['cardinality'] == 1) {
    $product = $wrapper->{$field_name}->value();
  }
  else {
    $product = commerce_product_reference_default_product($wrapper->{$field_name}->value());
  }

  return ctools_context_create('entity:commerce_product', $product);
}

/**
 * Settings form for the relationship.
 */
function commerce_product_reference_ctools_extra_default_product_from_node_settings_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $options = array();

  $fields = commerce_info_fields('commerce_product_reference', 'node');

  // Get labels.
  foreach ($fields as $field_name => $field) {
    $labels = array();

    foreach ($field['bundles']['node'] as $bundle) {
      $instance = field_info_instance('node', $field_name, $bundle);
      if (!in_array($labels, $instance['label'])) {
        $labels[] = $instance['label'];
      }
    }

    $options[$field_name] = t('@labels (!field_name)', array('@labels' => implode(', ', $labels), '!field_name' => $field_name));
  }

  $form['field'] = array(
    '#type' => 'select',
    '#title' => t('Product Reference field'),
    '#options' => $options,
    '#default_value' => $conf['field'],
    '#prefix' => '<div class="clearfix">',
    '#suffix' => '</div>',
  );

  return $form;
}
